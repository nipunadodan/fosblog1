<?php require_once 'config.php';?>
<!doctype html>
<html>
<head>
    <link rel="icon" type="image/png" href="favicon.png">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Nipuna Dodantenna">
    <meta name="theme-color" content="#65005E" />
    <meta name="apple-itunes-app" content="app-id="/>
    <meta name="google-play-app" content="app-id=">
    <meta name="google-site-verification" content="" />

    <title><?php echo SITE_NAME; ?></title>

    <!--<link href="<?php /*echo ASSETS_URL*/?>webfonts/sfui/sfui.css" rel="stylesheet" />-->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Asap+Condensed" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700" rel="stylesheet">
    <link href="<?php echo ASSETS_URL?>webfonts/malithi.css" rel="stylesheet" />
    <link href="<?php echo ASSETS_URL?>css/bootstrap.min.css" rel="stylesheet" />
    <!--<link href="<?php /*echo ASSETS_URL*/?>css/jquery-ui.min.css" rel="stylesheet">-->
    <link href="<?php echo ASSETS_URL?>css/intlTelInput.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS_URL?>css/datetimepicker.min.css" rel="stylesheet">
    <link href="<?php echo ASSETS_URL?>css/swiper.min.css" rel="stylesheet">
    <!--<link href="<?php /*echo ASSETS_URL*/?>css/fresh-bootstrap-table.css" rel="stylesheet" />-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="<?php echo ASSETS_URL?>css/styles.css" rel="stylesheet" />

    <script src="<?php echo ASSETS_URL?>js/jquery.min.js"></script>
    <script src="<?php echo ASSETS_URL?>js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo ASSETS_URL?>js/datetimepicker.min.js"></script>
    <script src="<?php echo ASSETS_URL?>js/intlTelInput.min.js">
        <script>var site_url = "<?php echo SITE_URL; ?>"</script>
    <!--<script src="<?php /*echo ASSETS_URL*/?>js/ajax.js"></script>-->
    <!--<script src="js/jquery-ui.min.js"></script>-->
    <script src="<?php echo ASSETS_URL?>js/swiper.min.js"></script>
    <script src="<?php echo ASSETS_URL?>js/scripts.js"></script>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="56">
    <div id="content">
        <?php include_once TEMPLATE_PATH.'menu.php'; ?>
