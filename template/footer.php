            <footer id="main-footer">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8 p-5 text-center text-md-left">
                            <p class="text-small text-muted"><span class="footer-copy-section">All Rights Reserved</span> <span class="footer-copy-section">&copy; 2013 - <?php echo date('Y'); ?></span> <a class="footer-copy-section" href="https://fos.cmb.ac.lk/">FOS Media</a> <a class="footer-copy-section" href="https://science.cmb.ac.lk/">Faculty of Science</a> <a class="footer-copy-section" href="https://www.cmb.ac.lk/">University of Colombo</a></p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </body>
</html>
