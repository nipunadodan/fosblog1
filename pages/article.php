<article class="single-article">
    <header class="text-center text-white" style="background: url('<?php echo SITE_URL?>uploads/2019/03/kashmir.jpg'); background-position: center; background-attachment: fixed;">
        <div class="h-100 overlay-dark d-flex align-items-center justify-content-center">
            <h1 class="text-bold text-center">A Road Trip in Kashmir</h1>
        </div>
    </header>

    <section class="post-content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <ul class="meta-tags text-md-white text-sm-dark text-bold text-center">
                        <li>Travel</li>
                        <li>Photography</li>
                        <li class=""><img class="meta-author-thumb" src="assets/images/authors/nipuna.jpg"></li>
                        <li>Experiences</li>
                        <li>Abroad</li>
                    </ul>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-8 text-justify px-5 py-4 bg-white" id="post">
                    <div class="text-small font-italic text-center pb-4 text-muted">by Nipuna Dodantenna</div>
                    <p>ලෝරීම් ඉප්සම් යනු සරලව මුද්‍රණ හා අකුරු ඇමිනුම් කර්මාන්තයේ උදාහරණ අකුරු පෙළ වෙයි. 1500 ගණන්වල සිට මේ දක්වා ලෝරම් ඉප්සම් කර්මාන්තයේ සම්මත අකුරු පෙළ ලෙස භාවිතා වන්නට පටන් ගත්තේ නාඳුනන මුද්‍රණ ශිල්පියෙකු. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempus lectus eget scelerisque sagittis. Nullam et varius tellus. In maximus dictum ultrices. Proin non enim sit amet ante finibus aliquam. Fusce ultricies lorem a sapien accumsan, eu molestie purus ullamcorper. Nunc quam elit, volutpat vitae fermentum ac, vehicula nec felis. Proin vulputate arcu ac tristique iaculis. Vivamus ut magna tortor. ලෝරීම් ඉප්සම් යනු සරලව මුද්‍රණ හා අකුරු ඇමිනුම් කර්මාන්තයේ උදාහරණ අකුරු පෙළ වෙයි. 1500 ගණන්වල සිට මේ දක්වා ලෝරම් ඉප්සම් කර්මාන්තයේ සම්මත අකුරු පෙළ ලෙස භාවිතා වන්නට පටන් ගත්තේ නාඳුනන මුද්‍රණ ශිල්පියෙකු. Maecenas rutrum ex auctor sapien molestie, et tristique ipsum luctus. Donec dignissim, sem ut congue maximus, urna elit feugiat leo, eget egestas justo diam eget turpis. Nullam luctus ligula est, et tempus quam pretium at.</p>

                    <p>Sed venenatis auctor sem vehicula accumsan. Maecenas eu orci tempus, feugiat augue vitae, suscipit massa. Etiam ornare porttitor porttitor. Maecenas sodales velit est, aliquam facilisis ex posuere eu. Sed lobortis ex vel suscipit efficitur. <a href="#">Phasellus congue</a> tincidunt velit a cursus. Sed tincidunt in turpis id venenatis. Vivamus blandit pharetra orci ac lacinia. In hendrerit, lacus id iaculis vulputate, eros arcu sollicitudin ipsum, ullamcorper bibendum ante nibh vel tellus. Nullam id sem lacus. Donec et urna eget erat lobortis iaculis. Mauris eu velit lobortis, tempus mi a, aliquet dui.</p>

                    <pre class="">Vivamus ac metus eu dolor cursus lobortis. Proin at eleifend est, quis luctus lorem. Duis fermentum velit a sapien consequat vehicula. Ut id sapien iaculis, maximus sem ut, maximus nisl. Integer quis mi aliquam, fermentum mauris a, faucibus neque. Etiam eleifend molestie nulla, vel scelerisque sem aliquam in. Duis sodales lobortis commodo. Integer quis justo vel magna porttitor tempus et sit amet augue. Proin elit purus, tincidunt quis varius nec, euismod vitae risus.<br/><br/>SELECT * FROM timetable WHERE FROM_UNIXTIME($begin) BETWEEN starttime AND endtime OR FROM_UNIXTIME($end) BETWEEN starttime AND endtime;
                    </pre>

                    <p>ලෝරීම් ඉප්සම් යනු සරලව මුද්‍රණ හා අකුරු ඇමිනුම් කර්මාන්තයේ උදාහරණ අකුරු පෙළ වෙයි. 1500 ගණන්වල සිට <a href="#">මේ දක්වා</a> ලෝරම් ඉප්සම් කර්මාන්තයේ සම්මත අකුරු පෙළ ලෙස භාවිතා වන්නට පටන් ගත්තේ නාඳුනන මුද්‍රණ ශිල්පියෙකු යම් අකුරු මිශ්‍රණයක් රැගෙන ඒවායෙන් අකුරු ආදර්ශ පොතක් සෑදීමට ගත් පසුවය. එය ශතවර්ශ පහක් පමණ නොවී පැමිණ ඉලෙක්ට්‍රොනික යුගයටද පිවිසුණි. 1960 දී ලෝරම් ඉප්සම් කොටස් අඩංගු ලෙට්‍රාසෙට් තහඩු නිදහස් කිරීමත් සමඟ ජනප්‍රිය වන්නට පටන් ගෙන මෑතකදී පේජ් මේකර් වැනි මෘදුකාංග සමඟ ද පැමිණීම නිසා ලෝක ප්‍රසිද්ධියට පත්විය.</p>

                    <p>Nam et risus risus. Maecenas ante velit, semper eget felis sit amet, laoreet vulputate augue. Integer pulvinar vestibulum eros, id vulputate lectus laoreet eu. Pellentesque aliquam, est in aliquet lacinia, mi magna pulvinar arcu, sollicitudin faucibus sem lectus non elit. Pellentesque risus metus, auctor non ullamcorper ut, aliquet nec dolor. Phasellus et nisl viverra, sodales velit et, ornare nulla. Praesent faucibus ex velit, ut facilisis nisi varius ut. Mauris elementum sit amet mauris sed hendrerit. Aliquam quis iaculis justo, molestie condimentum nisl. Nullam quis augue congue, ullamcorper est eu, luctus diam.</p>

                    <img class="img-inner-100" src="uploads/2019/03/kashmir.jpg">
                    <p class="text-small font-italic">Figure: Zojila Pass</p>

                    <p>Pellentesque vulputate molestie lorem, id maximus orci maximus ut. Pellentesque in rutrum eros. Quisque pulvinar massa turpis, sit amet laoreet neque dictum sed. Nunc malesuada turpis feugiat enim auctor, id congue metus rutrum. Praesent aliquet velit non tellus consequat consequat. Maecenas accumsan dolor sit amet massa consequat, at sodales sem semper. Sed iaculis a velit at facilisis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Cras velit augue, ultricies at augue eget, feugiat lacinia tellus. Donec a euismod purus. Sed hendrerit varius dui non elementum. Integer rhoncus orci id ante maximus consequat. Donec dignissim ante et ipsum consectetur interdum.</p>

                    <p>Curabitur molestie fermentum urna, et blandit lectus molestie quis. Curabitur lobortis, risus ut hendrerit viverra, mauris urna finibus enim, at commodo nibh nibh vitae nibh. Quisque dictum tortor quis faucibus accumsan. Praesent eros nulla, tempor vitae ante ac, iaculis mattis turpis. Fusce vel justo vel velit eleifend aliquet. Integer at interdum purus. Aliquam at diam pulvinar, viverra quam a, faucibus sem.</p>

                    <div class="alignfull">
                        <img class="img-outer-100" src="uploads/2019/03/kashmir.jpg">
                    </div>
                        <p class="text-small font-italic">Figure: Zojila Pass</p>

                    <p>Sed eu tincidunt nisi, id posuere neque. Maecenas accumsan lectus diam, in finibus ipsum dapibus ut. Curabitur non auctor tellus. Maecenas facilisis bibendum imperdiet. Vestibulum placerat ornare efficitur. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut aliquet facilisis nisl. Nulla facilisi. Praesent placerat, nibh ac gravida sodales, odio neque euismod ex, vel elementum leo neque quis tellus. Nulla nec quam ac mauris bibendum varius vehicula sed magna. Suspendisse commodo mattis blandit. Ut ex turpis, feugiat et faucibus ac, vehicula at orci. Nunc at neque non odio rutrum lobortis. Nulla facilisi. Ut nec neque eget dolor porta hendrerit. Donec interdum magna quis tortor sollicitudin pharetra.</p>

                    <pre class="alignfull">Vivamus ac metus eu dolor cursus lobortis. Proin at eleifend est, quis luctus lorem. Duis fermentum velit a sapien consequat vehicula. Ut id sapien iaculis, maximus sem ut, maximus nisl. Integer quis mi aliquam, fermentum mauris a, faucibus neque. Etiam eleifend molestie nulla, vel scelerisque sem aliquam in. Duis sodales lobortis commodo. Integer quis justo vel magna porttitor tempus et sit amet augue. Proin elit purus, tincidunt quis varius nec, euismod vitae risus.<br/><br/>SELECT * FROM timetable WHERE FROM_UNIXTIME($begin) BETWEEN starttime AND endtime OR FROM_UNIXTIME($end) BETWEEN starttime AND endtime;
                    </pre>

                    <p>Vivamus ac metus eu dolor cursus lobortis. Proin at eleifend est, quis luctus lorem. Duis fermentum velit a sapien consequat vehicula. Ut id sapien iaculis, maximus sem ut, maximus nisl. Integer quis mi aliquam, fermentum mauris a, faucibus neque. Etiam eleifend molestie nulla, vel scelerisque sem aliquam in. Duis sodales lobortis commodo. Integer quis justo vel magna porttitor tempus et sit amet augue. Proin elit purus, tincidunt quis varius nec, euismod vitae risus.</p>
                </div>
            </div>
        </div>

        <footer class="share-post">
            <div class="container">
                <div class="row text-center">
                    <div class="col-12 text-muted mb-5">
                        <p class="text-muted text-small text-underline">Share this</p>
                        <div class="footer-social-media mt-auto mb-4">
                            <a href="https://www.facebook.com/fosmedia/" title="Facebook - FOS Media" target="_blank"><i class="fab fa-facebook-f fa-2x fa-fw"></i></a>
                            <a href="https://twitter.com/fosmediar" title="Twitter - FOS Media" target="_blank"><i class="fab fa-twitter fa-2x fa-fw"></i></a>
                            <a href="https://www.instagram.com/fosmediar/" title="Instagram - FOS Media" target="_blank"><i class="fas fa-envelope fa-2x fa-fw"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row mb-5" id="comments">
                    <div class="col-md-12 text-center mb-5">
                        <button class="btn btn-outline-secondary rounded-0 text-uppercase text-small px-5 py-3">Comments</button>
                    </div>
                </div>
            </div>
        </footer>
    </section>

    <footer class="author bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 p-5">
                    <div class="row">
                        <div class="col-md-3">
                            <img class="meta-author-thumb img-fluid rounded-circle" src="assets/images/authors/nipuna.jpg">
                        </div>
                        <div class="col-md-9 text-center text-md-left">
                            <h3 class="text-thin">Nipuna Dodantenna</h3>
                            <p class="author-desc text-medium">
                                Vivamus ac metus eu dolor cursus lobortis. Proin at eleifend est, quis luctus lorem. Duis fermentum velit a sapien consequat vehicula. Ut id sapien iaculis, maximus sem ut, maximus nisl. Integer quis mi aliquam, fermentum mauris a, faucibus neque. Etiam eleifend molestie nulla, vel scelerisque sem aliquam in.
                            </p>
                            <footer class="share-post">
                                <div class="row">
                                    <div class="col-12 text-muted">
                                        <p class="text-muted text-small text-underline">Follow me</p>
                                        <div class="footer-social-media mt-auto mb-4">
                                            <a href="https://www.facebook.com/fosmedia/" title="Facebook - FOS Media" target="_blank"><i class="fab fa-facebook-f fa-2x fa-fw"></i></a>
                                            <a href="https://twitter.com/fosmediar" title="Twitter - FOS Media" target="_blank"><i class="fab fa-twitter fa-2x fa-fw"></i></a>
                                            <a href="https://www.instagram.com/fosmediar/" title="Instagram - FOS Media" target="_blank"><i class="fab fa-instagram fa-2x fa-fw"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</article>

