$(document).ready(function (e) {
    var windowWidth = $(window).innerWidth();
    var windowHeight = $(window).innerHeight();

    $('.img-outer-100').width(windowWidth)

    $(document).scroll(function () {
        var $nav = $(".navbar");
        $nav.toggleClass('scrolled', $(this).scrollTop() > 60);
        $nav.toggleClass('navbar-dark', $(this).scrollTop() < 60);
    });

    $('button.navbar-toggler').click(function () {
        /*if(('nav.navbar div.navbar-collapse').hasClass('show')){
            $(this).parents('nav.navbar').toggleClass('scrolled');
            $(this).parents('nav.navbar').toggleClass('navbar-dark');
        }*/
            $('nav.navbar').toggleClass('scrolled').toggleClass('navbar-dark');
    })
});